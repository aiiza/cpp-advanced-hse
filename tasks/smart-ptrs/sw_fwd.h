#pragma once

template <typename T>
class SharedPtr;

template <typename T>
class WeakPtr;
